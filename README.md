Hello world! 

# What is the relationship between Zipline and Quantopian?
   Zipline is a project, while Quantopian is a company that is backed by 
   investors. So zipline is an open-sourced backtester that is used both 
   independently and in Q's over products.

# What does the set_slippage function do?
   The set_slippage function calculates the realistic impact of orders
   on the execution price recived. Slippage must be defined in the initialize method. 
   It has no effect if defined elsewhere in the algorithm.
   If you do not specify a slippage method, slippage defaults to 
   VolumeShareSlippage(volume_limit=0.025, price_impact=0.1 (around 0.0065% I think) 
   (one can take up to 2.5% of a minute's trade volume).


# What is this handle_data function doing?
   The handle_data function is a algorithm that uses 'data' to get individual
   prices and windows of prices for one or more assets. Its an optional
   method that is called every minute. 
  
# Is the _test_args function part of the zipline framework?
   Could not find anything in the documentation, so probably not, but not sure.
   